﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioManager : MonoBehaviour
{
    static AudioManager _instance;

    public UnityAction PlayClick;

    public AudioClip bgmClip;
    [SerializeField]
    bool startBGMOnAwake;

    AudioSource BGMSource;
    List<AudioSource> audioSourceList = new List<AudioSource>();

    [SerializeField]
    GameObject audioSourceObject;

    [SerializeField, Range(5, 100)]
    int audioSourceCount = 5;
    [SerializeField]
    List<bool> audioSourceIsPlaying = new List<bool>();

    [SerializeField, Range(0f, 2f)]
    float transitionDuration = 0.5f;
    [SerializeField]
    bool isTransitioningBGM = false;

    [Range(0, 1)]
    public float Volume = 1f;

    /// <summary>
    /// Lista con los sonidos
    ///  0: Player Jump
    ///  1: Player Hurt
    ///  2: Player Shoot
    ///  3: Player Faint
    /// </summary>
    [SerializeField]
    List<AudioClip> sfxList = new List<AudioClip>();
    /// <summary>
    /// 0: Nada lmao
    /// </summary>
    [SerializeField]
    List<AudioClip> bgmList = new List<AudioClip>();

    public AudioClip DonCubetazo
    {
        get { return sfxList[0]; }
    }
    public AudioClip KatCubetazo
    {
        get { return sfxList[1]; }
    }
    public AudioClip DonDefault
    {
        get { return sfxList[2]; }
    }
    public AudioClip KatDefault
    {
        get { return sfxList[3]; }
    }

    public static AudioManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;
        DontDestroyOnLoad(gameObject);

        AddAudioSources();

        BGMSource = transform.GetChild(0).GetComponent<AudioSource>();
        for (int i = 1; i < transform.childCount; i++)
        {
            audioSourceList.Add(transform.GetChild(i).GetComponent<AudioSource>());
            audioSourceIsPlaying.Add(audioSourceList[i - 1].isPlaying);
        }
        BGMSource.clip = bgmClip;
        BGMSource.volume = Volume;
        if (startBGMOnAwake)
        {
            BGMSource.Play();
        }

        //PlayClick += Play(MenuClick);
    }

    void AddAudioSources()
    {
        for (int i = 0; i < audioSourceCount; i++)
        {
            GameObject obj = GameObject.Instantiate(audioSourceObject, transform);
            obj.name = $"AudioSource{i + 1}";
        }
    }

    private void Update()
    {
        for (int i = 0; i < audioSourceList.Count; i++)
        {
            audioSourceIsPlaying[i] = audioSourceList[i].isPlaying;
        }
    }

    public void StopBGM()
    {
        BGMSource.Stop();
    }
    public void PlayBGM()
    {
        BGMSource.Play();
    }
    public void ChangeBGM(AudioClip newBGM, float durationOverride = -1)
    {
        if (!isTransitioningBGM)
        {
            Debug.Log($"Changing BGM from {BGMSource.clip.name} to {newBGM.name}");
            StartCoroutine(SwitchBGM(newBGM, durationOverride != -1 ? durationOverride : transitionDuration));
        }
    }
    public void ChangeVolume(float newVolume)
    {
        BGMSource.volume = newVolume;
        foreach(AudioSource auSour in audioSourceList)
        {
            auSour.volume = newVolume;
        }
    }
    IEnumerator SwitchBGM(AudioClip newBGM, float duration)
    {
        isTransitioningBGM = true;
        yield return fadeSource(BGMSource, BGMSource.volume, 0, duration);
        StopBGM();
        BGMSource.clip = newBGM;
        PlayBGM();
        yield return fadeSource(BGMSource, BGMSource.volume, 1, duration);
        isTransitioningBGM = false;
    }

    IEnumerator fadeSource(AudioSource toFade, float initialValue, float targetValue, float duration)
    {
        float currentTime = 0;
        float currentProgress = 0;
        do
        {
            if (duration != 0)
            {
                currentProgress = currentTime / duration;
            }
            else
            {
                currentProgress = 1;
            }

            toFade.volume = Mathf.SmoothStep(initialValue, targetValue, currentProgress);

            currentTime += Time.unscaledDeltaTime;
            yield return null;
        } while (currentTime < duration + 0.1f);
    }

    public void Play(AudioClip auClip, float volumeOverride = -1)
    {
        if (HasAvailableAudioSource())
        {
            StartCoroutine(PlayClip(GetAvailableAudioSource(), auClip, volumeOverride == -1 ? Volume : volumeOverride));
        }
    }

    IEnumerator PlayClip(AudioSource audSource, AudioClip auClip, float volume)
    {
        audSource.clip = auClip;
        audSource.volume = volume;
        audSource.Play();
        yield return null;
        do
        {
            //Debug.Log($"isPlaying {audSource.isPlaying}, ({audSource.time}, {audSource.clip.length})");
            yield return null;
        } while (audSource.isPlaying);

        audSource.clip = null;
    }

    AudioSource GetAvailableAudioSource()
    {
        return audioSourceList.Find(x => !x.isPlaying);
    }
    bool HasAvailableAudioSource()
    {
        foreach (AudioSource auso in audioSourceList)
        {
            if (!auso.isPlaying)
            {
                return true;
            }
        }
        return false;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitsoundPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    AudioClip katCubetazo;
    AudioClip donCubetazo;
    AudioClip katDefault ;
    AudioClip donDefault ;

    [SerializeField]
    bool isCubetazo = true;

    static HitsoundPlayer _instance;

    public static HitsoundPlayer Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        _instance = this;
    }
    void Start()
    {
        katCubetazo = AudioManager.Instance.KatCubetazo;
        donCubetazo = AudioManager.Instance.DonCubetazo;
        katDefault = AudioManager.Instance.KatDefault;
        donDefault = AudioManager.Instance.DonDefault;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void ToggleHitsound()
    {
        isCubetazo = !isCubetazo;
    }
    public void PlayKat()
    {
        AudioManager.Instance.Play(isCubetazo ? katCubetazo : katDefault);
    }
    public void PlayDon()
    {
        AudioManager.Instance.Play(isCubetazo ? donCubetazo : donDefault);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchRay : MonoBehaviour
{
    //public bool debugUpdate = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if (debugUpdate)
        //{
        //    Debug.Log($"I am debugging! {Time.time:F4}");
        //}
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch currentTouch = Input.GetTouch(i);
                if (currentTouch.phase == TouchPhase.Began)
                {
                    HandleTouch(currentTouch.position);
                }
            }
        }
    }
    void HandleTouch(Vector2 pos)
    {
        //Debug.Log($"Hitting! {pos:F4} {Time.time:F4}");
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector2 rayOrigin = Camera.main.ScreenToWorldPoint(pos);
        //Ray ray = Camera.main.ScreenPointToRay(pos);
        //Debug.Log($"raycasting to world pos: {ray.origin:F4} towards {ray.direction:F4}");
        //Debug.Log($"raycasting to world pos: {rayOrigin:F4}");
        //Debug.DrawRay(ray.origin, ray.direction, Color.red, 2f);
        //Debug.DrawRay(rayOrigin, Vector3.forward * 100f, Color.red, 2f);

        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector3.forward, 100f);
        if (hit)
        {
            //Debug.Log($"Hit something! ({hit.transform.name})");
            HitableObject aux = hit.transform.GetComponent<HitableObject>();
            if (aux != null)
            {
                aux.Hit();
            }
            //hit.transform.SendMessage(leftClick ? "LeftClick" : "RightClick", SendMessageOptions.DontRequireReceiver);
        }
        //else
        //{
        //    Debug.Log("Hit nothing lmao");
        //}
    }
}
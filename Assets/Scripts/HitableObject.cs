using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HitType { Don, Kat }
public class HitableObject : MonoBehaviour
{
    public HitType type;

    public void Hit()
    {
        if (type == HitType.Don)
        {
            HitsoundPlayer.Instance.PlayDon();
        }
        else
        {
            HitsoundPlayer.Instance.PlayKat();
        }
    }
}
